# Vagrant Prestashop

A Vagrant-based development environment for Prestashop (based on VCCW for Wordpress: https://github.com/vccw-team/vccw)

## How to use

#### Step 1
Install vagrant dependencies
```
vagrant plugin install vagrant-hostsupdater
```


#### Step 2
Copy the default YAML configuration file into `settings.yml`:

```
cp default-settings.yml settings.yml
```


#### Step 3
Edit the `settings.yml` file.
Some of the settings you should change:

* `hostname`: the domain name to use for locally deploying the app/site (usually has the `.test` suffix).
* `hostname_aliases`: the aliases to use to access the local deployment domain name.
* `title`: the name of your app/site.
* `content_repo`: the git repo url (SSH or HTTPS) to clone, which contains the app/site code to be deployed.
* `synced_folder`: the path on your machine where the repo should be cloned.
* `document_root`: the path on the guest vagrant machine where to sync with `synced_folder` (it's better to leave as `/var/www/html` if you don't want to change the apache settings).


#### Step 4
Creates/configure the vagrant guest machine.

```
vagrant up
```