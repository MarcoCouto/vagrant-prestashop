# encoding: utf-8
# vim: ft=ruby expandtab shiftwidth=2 tabstop=2

require 'yaml'

Vagrant.require_version '>= 1.8.6'

Vagrant.configure(2) do |config|

  _conf = YAML.load(
    File.open(
      File.join(File.dirname(__FILE__), 'provision/default.yml'),
      File::RDONLY
    ).read
  )

  if File.exists?(File.join(File.dirname(__FILE__), 'settings.yml'))
    _site = YAML.load(
      File.open(
        File.join(File.dirname(__FILE__), 'settings.yml'),
        File::RDONLY
      ).read
    )
    _conf.merge!(_site) if _site.is_a?(Hash)
  end

  # forcing config variables
  _conf["vagrant_dir"] = "/vagrant"

  config.vm.define _conf['hostname'] do |v|
  end

  config.vm.box = ENV['wp_box'] || _conf['wp_box']
  config.vm.box_version = "= " + _conf['wp_box_version'].to_s
  config.ssh.forward_agent = true

  config.vm.box_check_update = true

  config.vm.hostname = _conf['hostname']
  config.vm.network :private_network, ip: _conf['ip']
  #config.vm.network "forwarded_port", guest: 80, host: 8080

  config.vm.synced_folder _conf['synced_folder'], _conf['document_root'], 
    owner: "vagrant",
    group: "www-data",
    mount_options: ["dmode=775,fmode=664"],
    create: true

  if Vagrant.has_plugin?('vagrant-hostsupdater')
    config.hostsupdater.aliases = _conf['hostname_aliases']
    config.hostsupdater.remove_on_suspend = true
  end

  if Vagrant.has_plugin?('vagrant-vbguest')
    config.vbguest.auto_update = false
  end

  if File.exists?(File.join(File.dirname(__FILE__), 'provision-pre.sh')) then
    config.vm.provision :shell, :path => File.join( File.dirname(__FILE__), 'provision-pre.sh' )
  end

  config.vm.provider :virtualbox do |vb|
    vb.linked_clone = _conf['linked_clone']
    vb.name = _conf['hostname']
    vb.memory = _conf['memory'].to_i
    vb.cpus = _conf['cpus'].to_i
    if 1 < _conf['cpus'].to_i
      vb.customize ['modifyvm', :id, '--ioapic', 'on']
    end
    vb.customize ['modifyvm', :id, '--natdnsproxy1', 'on']
    vb.customize ['modifyvm', :id, '--natdnshostresolver1', 'on']
    vb.customize ['setextradata', :id, 'VBoxInternal/Devices/VMMDev/0/Config/GetHostTimeDisabled', 0]
  end

  config.vm.provision "ansible_local" do |ansible|
    ansible.compatibility_mode = "2.0"
    ansible.extra_vars = {
      vccw: _conf
    }
    ansible.playbook = "provision/playbook.yml"
  end

  config.vm.provision :shell, :path => "provision/bootstrap.sh"

  config.trigger.after :provision do |trigger|
    if _conf['content_repo'] != ''
      trigger.info = "Cloning wp-content"
      trigger.run = {inline: "bash -c 'rm -rf " + _conf['synced_folder'] + "&& git clone " + _conf['content_repo'].to_s + " " + _conf['synced_folder'] }
    end
  end
end
