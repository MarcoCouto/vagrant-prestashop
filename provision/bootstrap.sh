#!/bin/bash

sudo apt-get update

sudo apt-get install -y -qq php7.0-cli
sudo apt-get install -y -qq php7.0-common
sudo apt-get install -y -qq php7.0-curl
sudo apt-get install -y -qq php7.0-zip
sudo apt-get install -y -qq php7.0-gd
sudo apt-get install -y -qq php7.0-mysql
sudo apt-get install -y -qq php7.0-xml
sudo apt-get install -y -qq php7.0-mcrypt
sudo apt-get install -y -qq php7.0-mbstring
sudo apt-get install -y -qq php7.0-json
sudo apt-get install -y -qq php7.0-intl

sudo a2enmod rewrite