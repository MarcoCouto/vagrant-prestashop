---
- hosts: all
  vars:
    wp_cli_bin_url: https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli-nightly.phar
    composer_version: 1.3.1

  tasks:

    # Setup bash
    - name: Place a ~/.bash_profile
      become: no
      template:
        src: ./templates/.bash_profile
        dest: "{{ ansible_env.HOME }}/.bash_profile"
    - name: Place a ~/.bash.d/
      become: no
      file:
        path: "{{ ansible_env.HOME }}/.bash.d"
        state: directory
    - name: Place a ~/.bash.d/vccw.sh
      become: no
      template:
        src: ./templates/vccw.sh
        dest: "{{ ansible_env.HOME }}/.bash.d/vccw.sh"

    # Install Composer libraries
    - name: Download the Composer
      become: yes
      get_url:
        url: https://getcomposer.org/installer
        dest: /tmp/composer-setup.php
        mode: 0755
        force: yes
    - name: Run the Composer installer
      become: yes
      shell: "php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer --version={{ composer_version }}"
    - name: Create a ~/.composer/
      file:
        path: "{{ ansible_env.HOME }}/.composer/"
        state: directory
    - name: Place a composer.json
      template:
        src: ./templates/composer.json
        dest: "{{ ansible_env.HOME }}/.composer/composer.json"
        force: no
    - name: Install Composer libraries
      composer:
        command: require
        arguments: "{{ item }}"
        working_dir: "{{ ansible_env.HOME }}/.composer"
      with_items: "{{ vccw.composers | default([]) }}"
      ignore_errors: yes

    # Install Ruby gems
    - name: Place a ~/.gemrc
      become: no
      template:
        src: ./templates/.gemrc
        dest: "{{ ansible_env.HOME }}/.gemrc"
    - name: Install Wordmove
      become: no
      gem:
        name: "wordmove"
        user_install: yes
        version: "2.4.0"
      ignore_errors: yes
    - name: Install Ruby gems
      become: no
      gem:
        name: "{{ item }}"
        user_install: yes
      when: item != "wordmove"
      with_items: "{{ vccw.ruby_gems | default([]) }}"
      ignore_errors: yes

    # Install npm packages
    - name: Place a ~/.npmrc
      become: no
      template:
        src: ./templates/.npmrc
        dest: "{{ ansible_env.HOME }}/.npmrc"
    - name: Install npm packages
      become: no
      npm:
        name: "{{ item }}"
        global: yes
        executable: /usr/bin/npm
      with_items: "{{ vccw.npms | default([]) }}"
      ignore_errors: yes

    # Remove unused files if exists
    - name: Remove unused files if exists
      become: yes
      file:
        path: "{{ ansible_env.HOME }}/{{ item }}"
        state: absent
      with_items:
        - .wget-hsts
